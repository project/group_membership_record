<?php

namespace Drupal\group_membership_record_emails\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupType;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\group_membership_record\Event\GroupMembershipRecordEventType;

/**
 * Class GroupMembershipRecordEmailForm.
 */
class GroupMembershipRecordEmailForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

    $form = parent::form($form, $form_state);

    $group_membership_record_email = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $group_membership_record_email->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $group_membership_record_email->id(),
      '#machine_name' => [
        'exists' => '\Drupal\group_membership_record_emails\Entity\GroupMembershipRecordEmail::load',
      ],
      '#disabled' => !$group_membership_record_email->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $group_membership_record_email->status(),
    ];

    $types = GroupMembershipRecordType::loadMultiple();
    $options = [];
    foreach ($types as $type_id => $type) {
      $options[$type_id] = $type->label();
    }
    $form['types'] = [
      '#type' => 'select',
      '#title' => $this->t('Type(s)'),
      '#default_value' =>  $group_membership_record_email->get('types') ?? null,
      '#options' => $options,
      '#multiple' => TRUE,
    ];

    $types = GroupType::loadMultiple();
    $options = [];
    foreach ($types as $type_id => $type) {
      $options[$type_id] = $type->label();
    }
    $form['group_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Group type(s)'),
      '#default_value' =>  $group_membership_record_email->get('group_types') ?? NULL,
      '#options' => $options,
      '#multiple' => TRUE,
    ];

    $roles = $gmrRepositoryService->getAllNonInternalRoles();
    $options = [];
    foreach ($roles as $role_id => $role) {
      $options[$role_id] = $role->label();
    }
    $form['roles'] = [
      '#type' => 'select',
      '#title' => $this->t('Role(s)'),
      '#default_value' =>  $group_membership_record_email->get('roles') ?? NULL,
      '#options' => $options,
      '#multiple' => TRUE,
    ];

    $form['trigger'] = [
      '#type' => 'select',
      '#title' => $this->t('Event on record that triggers e-mail'),
      '#options' => [
        GroupMembershipRecordEventType::INSERT => 'Insert (create)',
        GroupMembershipRecordEventType::UPDATE => 'Update (save/change)',
        GroupMembershipRecordEventType::DELETE => 'Delete',
        GroupMembershipRecordEventType::START => 'Started (based on date range)',
        GroupMembershipRecordEventType::END => 'Ended (based on date range)',
        GroupMembershipRecordEventType::ENABLE => 'Enabled',
        GroupMembershipRecordEventType::DISABLE => 'Disabled',
      ],
      '#default_value' => $group_membership_record_email->get('trigger') ?? GroupMembershipRecordEventType::INSERT,
      '#required' => TRUE,
    ];

    $form['to'] = [
      '#type' => 'email',
      '#title' => $this->t('To'),
      '#default_value' => $group_membership_record_email->get('to') ?? '',
      '#description' => 'Leave blank to e-mail the record user.'
    ];

    $form['from_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From'),
      '#maxlength' => 255,
      '#default_value' => $group_membership_record_email->get('from_name') ?? \Drupal::config('system.site')->get('name'),
      '#required' => TRUE,
    ];

    $form['from_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Reply-to e-mail'),
      '#maxlength' => 255,
      '#default_value' => $group_membership_record_email->get('from_email') ?? \Drupal::config('system.site')->get('mail'),
      '#required' => TRUE,
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 255,
      '#default_value' => $group_membership_record_email->get('title') ?? '',
      '#required' => TRUE,
    ];

    $form['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $group_membership_record_email->get('body') ?? '',
      '#description' => 'You can include [group:url] to include the URL of group, [group:title] for the name, and [user:name] for the user\'s name.',
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $group_membership_record_email = $this->entity;
    $status = $group_membership_record_email->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label group membership record email.', [
          '%label' => $group_membership_record_email->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label group membership record email.', [
          '%label' => $group_membership_record_email->label(),
        ]));
    }
    $form_state->setRedirectUrl($group_membership_record_email->toUrl('collection'));
  }
}
