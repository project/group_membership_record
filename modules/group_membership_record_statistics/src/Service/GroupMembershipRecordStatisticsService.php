<?php

namespace Drupal\group_membership_record_statistics\Service;

use DateInterval;
use DatePeriod;
use DateTime;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupRole;
use Drupal\user\UserInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRoleInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\group_membership_record\Entity\GroupMembershipRecordTypeInterface;

/**
 * Class GroupMembershipRecordStatisticsService
 */
class GroupMembershipRecordStatisticsService {

  /**
   * Constructs a new GroupMembershipRecordRepository object.
   */
  public function __construct() {
  }

  /**
   * @param DateTime $startDate
   * @param DateTime $endDate
   * @param string $granularity = D M or Y
   * @param array of GroupRole $groupRoles
   * @param NULL $recordTypes
   *
   * @return [type]
   */
  public function getCountsForDates(DateTime $startDate, DateTime $endDate, $granularity = 'M', $groupRoles = NULL, $recordTypes = NULL) {
    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');

    $results = [];

    $interval = new DateInterval('P1' + $granularity);
    $daterange = new DatePeriod($startDate, $interval, $endDate);
    foreach ($daterange as $date) {
      $count = 0;
      if ($groupRoles) {
        foreach ($groupRoles as $role) {
          $count += $gmrRepositoryService->count(NULL, NULL, $role, NULL, $date);
        }
      } elseif ($recordTypes) {
        foreach ($recordTypes as $type) {
          $count += $gmrRepositoryService->count(NULL, NULL, NULL, $type, $date);
        }
      } else {
        $count += $gmrRepositoryService->count(NULL, NULL, NULL, NULL, $date);
      }

      $year = $date->format('Y');
      $month = $date->format('m');
      $day = $date->format('d');
      if (!isset($results[$year])) {
        $results[$year] = [];
      }

      if ($granularity == 'Y') {
        $results[$year] = $count;
      } else if ($granularity == 'M') {
        $results[$year][$month] = $count;
      } elseif ($granularity == 'D') {
        if (!isset($results[$year][$month])) {
          $results[$year][$month] = [];
        }
        $results[$year][$month][$day] = $count;
      }
    }

    return $results;
  }
}
