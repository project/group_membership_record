<?php

namespace Drupal\group_membership_record;

use Drupal;
use Drupal\Core\Cache\Cache;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;

/**
 * Provides dynamic permissions for Group membership record of different types.
 *
 * @ingroup group_membership_record
 *
 */
class GroupMembershipRecordPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of node type permissions.
   *
   * @return array
   *   The GroupMembershipRecord by bundle permissions.
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function generatePermissions() {
    $cid = __METHOD__;
    if ($item = Drupal::cache()->get($cid)) {
      return $item->data;
    }

    $perms = [];

    foreach (GroupMembershipRecordType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    Drupal::cache()
      ->set($cid, $perms, Cache::PERMANENT, ['config:group_membersghip_record_type_list']);
    return $perms;
  }

  /**
   * Returns a list of node permissions for a given node type.
   *
   * @param \Drupal\group_membership_record\Entity\GroupMembershipRecord $type
   *   The GroupMembershipRecord type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(GroupMembershipRecordType $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "$type_id create entities" => [
        'title' => $this->t('Create new %type_name', $type_params),
      ],
      "$type_id edit own entities" => [
        'title' => $this->t('Edit own %type_name', $type_params),
      ],
      "$type_id edit any entities" => [
        'title' => $this->t('Edit any %type_name', $type_params),
      ],
      "$type_id delete own entities" => [
        'title' => $this->t('Delete own %type_name', $type_params),
      ],
      "$type_id delete any entities" => [
        'title' => $this->t('Delete any %type_name', $type_params),
      ],
    ];
  }

}
