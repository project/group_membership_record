<?php

namespace Drupal\group_membership_record\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRoleInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Entity\GroupMembershipRecordInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecordType;
use Drupal\group_membership_record\Entity\GroupMembershipRecordTypeInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides group membership record route controllers.
 */
class GroupMembershipRecordController extends ControllerBase
{

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * Constructs a new GroupMembershipController.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   The entity form builder.
   */
  public function __construct(AccountInterface $current_user, EntityFormBuilderInterface $entity_form_builder)
  {
    $this->currentUser = $current_user;
    $this->entityFormBuilder = $entity_form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('current_user'),
      $container->get('entity.form_builder')
    );
  }

  public function checkJoinAccess(GroupInterface $group, GroupRoleInterface $group_role)
  {

    $account = User::load($this->currentUser()->id());

    // Must have group join access
    $access = GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'join group');

    // Role must be joinable this way (set on role settings)
    $access = $access->andIf(AccessResult::allowedIf($group_role->getThirdPartySetting('group_membership_record', 'can_join_via_block') == TRUE));

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $alreadyJoined = $gmrRepositoryService->currentEnabledInstanceExists($account, $group, $group_role);
    $access = $access->andIf(AccessResult::allowedIf(!$alreadyJoined));

    return $access;
  }

  /**
   * Provides the form for joining a group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to join.
   *
   * @return array
   *   A group join form.
   */
  public function join(GroupInterface $group, GroupRoleInterface $group_role)
  {
    $group_membership_record_type = GroupMembershipRecordType::load($group_role->getThirdPartySetting('group_membership_record', 'group_membership_record_type'));
    if (!$group_membership_record_type) {
      \Drupal::messenger()->addMessage("No record type for role: " . ($group_role ? $group_role->id() : 'null'), 'error');
      return new RedirectResponse($_SERVER['HTTP_REFERER']);
    }

    // Pre-populate a group membership record with the current user.
    $gmr = GroupMembershipRecord::create([
      'type' => $group_membership_record_type->id(),
      'group_id' => $group->id(),
      'group_role_id' => $group_role->id(),
      'user_id' => $this->currentUser->id(),
      'date_range' => [
        'value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, \Drupal::time()->getRequestTime()),
        'end_value' => ''
      ]
    ]);

    $form = $this->entityFormBuilder->getForm($gmr, 'join', ['join_form' => true]);
    return $form;
  }

  /**
   * The _title_callback for the join form route.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to join.
   *
   * @return string
   *   The page title.
   */
  public function joinTitle(GroupInterface $group, GroupRoleInterface $group_role)
  {
    $text = 'Join %group';
    if ($group_role->getThirdPartySetting('group_membership_record', 'join_form_title') && $group_role->getThirdPartySetting('group_membership_record', 'join_form_title') != '') {
      $text = $group_role->getThirdPartySetting('group_membership_record', 'join_form_title');
    }
    return $this->t($text, ['%group' => $group->label()]);
  }

  /**
   * Provides the form for leaving a group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to leave.
   *
   * @return array
   *   A group leave form.
   */
  public function leave(GroupInterface $group)
  {
    $group_content = $group->getMember($this->currentUser)->getGroupContent();
    return $this->entityFormBuilder->getForm($group_content, 'group-leave');
  }



  /**
   * Provides the form for joining a group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to join.
   *
   * @return array
   *   A group join form.
   */
  public function end(GroupInterface $group, GroupMembershipRecordInterface $group_membership_record)
  {
    $form = $this->entityFormBuilder->getForm($group_membership_record, 'end', ['end_form' => true]);
    return $form;
  }

  /**
   * The _title_callback for the end form route.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to join.
   *
   * @return string
   *   The page title.
   */
  public function endTitle(GroupInterface $group, GroupMembershipRecordInterface $group_membership_record)
  {
    $text = 'End %role in %group';
    return $this->t($text, [
      '%role' => $group_membership_record->type->entity->label(),
      '%group' => $group->label(),
    ]);
  }

  public function checkEndAccess(GroupInterface $group, GroupMembershipRecordInterface $group_membership_record)
  {

    $account = User::load($this->currentUser()->id());

    // Must have group end access
    $access = GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'end group membership records');

    // Must be current
    $access = $access->andIf(AccessResult::allowedIf($group_membership_record->isCurrent()));

    return $access;
  }
}
