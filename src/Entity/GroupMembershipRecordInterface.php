<?php

namespace Drupal\group_membership_record\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Group membership record entities.
 *
 * @ingroup group_membership_record
 */
interface GroupMembershipRecordInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface
{

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Group membership record name.
   *
   * @return string
   *   Name of the Group membership record.
   */
  public function getName();

  /**
   * Sets the Group membership record name.
   *
   * @param string $name
   *   The Group membership record name.
   *
   * @return \Drupal\group_membership_record\Entity\GroupMembershipRecordInterface
   *   The called Group membership record entity.
   */
  public function setName($name);

  /**
   * Gets the Group membership record creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Group membership record.
   */
  public function getCreatedTime();

  /**
   * Sets the Group membership record creation timestamp.
   *
   * @param int $timestamp
   *   The Group membership record creation timestamp.
   *
   * @return \Drupal\group_membership_record\Entity\GroupMembershipRecordInterface
   *   The called Group membership record entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * {@inheritdoc}
   */
  public function isCurrent();

  /**
   * {@inheritdoc}
   */
  public function isEnabled();

  public function getCurrency();

  public function getGroup();

  public function getRole();

  public function getUser();
}
