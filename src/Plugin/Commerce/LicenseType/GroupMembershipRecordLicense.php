<?php

//https://git.drupalcode.org/project/commerce_license_group/-/blob/1.0.x/src/Plugin/Commerce/LicenseType/GroupMembership.php

namespace Drupal\group_membership_record\Plugin\Commerce\LicenseType;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_license\Entity\LicenseInterface;
use Drupal\commerce_license\ExistingRights\ExistingRightsResult;
use Drupal\commerce_license\Plugin\Commerce\LicenseType\LicenseTypeBase;
use Drupal\commerce_license\Plugin\Commerce\LicenseType\ExistingRightsFromConfigurationCheckingInterface;
use Drupal\commerce_license\Plugin\Commerce\LicenseType\GrantedEntityLockingInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group\GroupMembership as GroupMembershipEntity;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\group_membership_record\Entity\GroupMembershipRecordInterface;

/**
 * Allow purchasing an enabled group membership record via commerce license
 *
 * This will create an enabled instance when the license is granted
 * and disable that instance when revoked (i.e. set enabled = false and set end date).
 * It will not delete the instance, so it can remain a record in the group.
 *
 * Optionally add field_group_id referencing groups, and field_group_role_id referencing group roles,
 * on to the Product entity to have any variations default to what's set there
 *
 * @CommerceLicenseType(
 *   id = "group_membership_record",
 *   label = @Translation("Group membership record"),
 * )
 */
class GroupMembershipRecordLicense extends LicenseTypeBase implements ExistingRightsFromConfigurationCheckingInterface, GrantedEntityLockingInterface {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(LicenseInterface $license) {
    $args = [
      '@group' => $license->license_group->entity->label(),
      '@group_role' => $license->license_group_role->entity->label(),
    ];
    return $this->t('@group - @group_role role instance license', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'license_group' => '',
      'license_group_role' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function grantLicense(LicenseInterface $license) {
    /** @var \Drupal\group\Entity\GroupInterface $group */
    $group = $license->license_group->entity;
    $group_role = $license->license_group_role->entity;

    if ($group instanceof \Drupal\group\Entity\GroupInterface && $group_role instanceof \Drupal\group\Entity\GroupRoleInterface) {
      // Get the owner of the license and grant them group membership.
      $owner = $license->getOwner();

      // Set values
      $values = [
        'enabled' => true,
        'date_range' => [
          'value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $license->getGrantedTime()),
          'end_value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $license->getExpiresTime()),
        ],
        'source' => [
          ['target_id' => $license->id(), 'target_type' => 'commerce_license']
        ]
      ];
      $record = \Drupal::service('group_membership_record.manager')->create($owner, $group, $group_role, $values);
    } else {
      \Drupal::logger('group_membership_record')->error("Couldn't get group and/or role for license " . $license->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function revokeLicense(LicenseInterface $license) {
    // Allow loading multiple here in case in future one license leads to many GRIs
    $records = \Drupal::service('group_membership_record.repository')->getBySourceId('commerce_license', $license->id());

    foreach ($records as $record) {
      // Disable the group membership record
      $record->set('enabled', false);
      // End it now
      $record->set('date_range', [
        'value' => $record->get('date_range')->value,
        'end_value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, \Drupal::time()->getRequestTime())
      ]);

      $record->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkUserHasExistingRights(UserInterface $user) {
    /** @var \Drupal\group\Entity\Group $group */
    $group = \Drupal::service('entity_type.manager')->getStorage('group')->load($this->configuration['license_group']);

    if (empty($group)) {
      return ExistingRightsResult::rightsDoNotExist();
    }

    /** @var \Drupal\group\Entity\GroupRole $groupRole */
    $groupRole = \Drupal::service('entity_type.manager')->getStorage('group_role')->load($this->configuration['license_group_role']);

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $currentInstanceExists = $gmrRepositoryService->currentEnabledInstanceExists($user, $group, $groupRole);

    return ExistingRightsResult::rightsExistIf(
      $currentInstanceExists,
      $this->t("You already have a current and enabled '@group_role' role for @group.", [
        '@group' => $group->label(),
        '@group_role' => $groupRole->label(),
      ]),
      $this->t("User @user already has a current and enabled '@group_role' role for @group.", [
        '@user' => $user->getDisplayName(),
        '@group' => $group->label(),
        '@group_role' => $groupRole->label(),
      ])
    );
  }

  /**
   * {@inheritdoc}
   */
  public function alterEntityOwnerForm(&$form, FormStateInterface $form_state, $form_id, LicenseInterface $license, EntityInterface $form_entity) {
    if ($form_entity->getEntityTypeId() == 'group_membership_record') {
      if ($form_entity->getGroup()->id() == $license->license_group->entity->id()) {
        \Drupal::messenger()->addWarning("This group membership record is granted by a purchased license. It should not be edited or removed manually.");
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // @todo maybe allow filtering by group type (or only allow groups of types that have been enabled to allow a license to use them in the group settings?)
    $form['license_group'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'group',
      '#title' => $this->t('Group to add the user to'),
      '#default_value' => $this->configuration['license_group'] ? Group::load($this->configuration['license_group']) : null,
      '#required' => TRUE
    ];

    // @todo limit roles to the type of the selected group
    $form['license_group_role'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'group_role',
      '#title' => $this->t('Group role to grant'),
      '#default_value' => $this->configuration['license_group_role'] ? GroupRole::load($this->configuration['license_group_role']) : null,
      '#required' => TRUE,
      '#selection_handler' => 'group_role:not_internal'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);

    $this->configuration['license_group'] = $values['license_group'];
    $this->configuration['license_group_role'] = $values['license_group_role'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['license_group'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel(t('Group'))
      ->setDescription(t('The group this product grants membership of.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'group')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 1,
        'settings' => [
          'link' => TRUE,
        ],
      ]);

    $fields['license_group_role'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel(t('Group role'))
      ->setDescription(t('The role this product grants in the groop.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'group_role')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 1,
        'settings' => [
          'link' => TRUE,
        ],
      ]);

    return $fields;
  }
}
