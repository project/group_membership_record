<?php

namespace Drupal\group_membership_record\Plugin\QueueWorker;

use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;

/**
 * Commerce Stock Local location level update worker.
 *
 * @QueueWorker(
 *   id = "group_membership_record_syncer",
 *   title = @Translation("Group membership record - group role syncing"),
 *   cron = {"time" = 60}
 * )
 */
class RecordSync extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $recordIds = $data;

    foreach ($recordIds as $recordId) {
      $record = GroupMembershipRecord::load($recordId);

      // Some enabled determiners may use the dates
      $record->updateEnabled();
      $record->sync();
      unset($record);
    }

  }

}
