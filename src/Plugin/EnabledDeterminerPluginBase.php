<?php

namespace Drupal\group_membership_record\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * A base class to help developers implement their own plugins.
 *
 * @see \Drupal\group_membership_record\Annotation\EnabledDeterminerPlugin
 * @see \Drupal\group_membership_record\EnabledDeterminerPluginInterface
 */
abstract class EnabledDeterminerPluginBase extends PluginBase implements EnabledDeterminerPluginInterface, ContainerFactoryPluginInterface
{

    /**
     * This text shows under the 'Enabled' checkbox if it's shown on a form where it cannot be changed due to being determined by a plugin.
     */
    public $messageForFormEnabledField = 'Whether or not this instance is enabled is determined by a programmed set of rules for this instance type, and cannot be changed by the user.';

    /**
     * {@inheritdoc}
     */
    public function description()
    {
        // Retrieve the @description property from the annotation and return it.
        return $this->pluginDefinition['description'];
    }

    public function getInstanceTypes()
    {
        return $this->pluginDefinition['group_membership_record_types'];
    }

    public function containsInstanceType($group_membership_record_type)
    {
        return in_array($group_membership_record_type, $this->pluginDefinition['group_membership_record_types']);
    }

    /**
     * {@inheritdoc}
     */
    public function settings($setting_key = null)
    {
        if (!is_null($setting_key)) {
            return
                isset($this->pluginDefinition['settings'][$setting_key]) ?
                $this->pluginDefinition['settings'][$setting_key] :
                $this->pluginDefinition['settings'];
        }
        return $this->pluginDefinition['settings'];
    }

    /**
     * Returns whether the instance should be enabled or not
     * 
     * Defaults to leaving the field exactly as it is.
     * 
     * In a custom module, you might create a plugin that sets this value differently,
     * e.g. based on custom fields that you've added on the group membership record type
     *
     * @param GroupMembershipRecord $record
     *
     * @return boolean
     */
    abstract public function determineEnabledValue(GroupMembershipRecord $record);

    /** 
     * Returns an array of explanations of why the instance is enabled or not
     * 
     * For use in views, maybe access denied pages etc.
     * 
     * @param GroupMembershipRecord $record
     *
     * @return array of strings
     */
    abstract public function getEnabledExplanation(GroupMembershipRecord $record);
}
