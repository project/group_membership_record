<?php

namespace Drupal\group_membership_record\Plugin\migrate\destination;

use Drupal\group_membership_record\Entity\GroupMembershipRecord as EntityGroupMembershipRecord;
use Drupal\group_membership_record\Plugin\migrate\destination\GroupMembershipRecord as DestinationGroupMembershipRecord;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Row;

/**
 * @MigrateDestination(
 *   id = "group_membership_record"
 * )
 */
class GroupMembershipRecord extends EntityContentBase
{

  /**
   * {@inheritdoc}
   */
  protected static function getEntityTypeId($plugin_id)
  {
    return 'group_membership_record';
  }

  /** @var string $entityType */
  public static $entityType = 'group_membership_record';

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = [])
  {
    $result = parent::import($row, $old_destination_id_values);

    foreach ($result as $id) {
      $group_membership_record = EntityGroupMembershipRecord::load($id);
      $group_membership_record->sync();
    }

    return $result;
  }
}
