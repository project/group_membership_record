<?php

namespace Drupal\group_membership_record\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * Only shows the group roles which are not internal 
 * (internal roles are used by Group to manage site roles,
 * so not relevant for groups from a user point of view)
 *
 * @EntityReferenceSelection(
 *   id = "group_role:not_internal",
 *   label = @Translation("Group role selection excluding internal roles"),
 *   entity_types = {"group_role"},
 *   group = "group_role",
 *   weight = 0
 * )
 */
class GroupRoleNotInternalSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {

    $query = parent::buildEntityQuery($match, $match_operator);
    $query->condition('internal', 0, '=');

    return $query;
  }

}
