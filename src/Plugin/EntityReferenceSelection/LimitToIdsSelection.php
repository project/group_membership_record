<?php

namespace Drupal\group_membership_record\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * @EntityReferenceSelection(
 *   id = "group_membership_record:limit_to_ids",
 *   label = @Translation("Limit autocomplete to specified IDs"),
 *   entity_types = {"group", "group_role", "user"},
 *   group = "group_membership_record",
 *   weight = 0
 * )
 */
class LimitToIdsSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $configuration = $this->getConfiguration();

    $query = parent::buildEntityQuery($match, $match_operator);
    $query->condition('id', $configuration['ids'], 'IN');

    return $query;
  }
}
