<?php

namespace Drupal\group_membership_record\Plugin;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\group_membership_record\Annotation\EnabledDeterminerPlugin;

/**
 * A plugin manager.
 * 
 * https://www.drupal.org/docs/drupal-apis/plugin-api/creating-your-own-plugin-manager
 * https://medium.com/@uditrawat/custom-plugin-type-in-drupal-8-5c243b4ed152
 *
 * Extends the DefaultPluginManager to provide
 * a way to manage group membership record plugins. A plugin manager defines a new plugin type
 * and how instances of any plugin of that type will be discovered, instantiated
 * and more.
 * 
 * @see \Drupal\group_membership_record\Annotation\EnabledDeterminerPlugin
 * @see \Drupal\group_membership_record\EnabledDeterminerPluginInterface
 * @see plugin_api
 */
class EnabledDeterminerPluginManager extends DefaultPluginManager
{
    /**
     * Constructs a ArchiverManager object.
     *
     * @param \Traversable $namespaces
     *   An object that implements \Traversable which contains the root paths
     *   keyed by the corresponding namespace to look for plugin implementations.
     * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
     *   Cache backend instance to use.
     * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
     *   The module handler to invoke the alter hook with.
     */
    public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler)
    {
        parent::__construct(
            'Plugin/GroupMembershipRecord/EnabledDeterminer',
            $namespaces,
            $module_handler,
            'Drupal\group_membership_record\Plugin\EnabledDeterminerPluginInterface',
            'Drupal\group_membership_record\Annotation\EnabledDeterminerPlugin'
        );
        $this->alterInfo('group_membership_record_enabled_determiner_info');
        $this->setCacheBackend($cache_backend, 'group_membership_record_enabled_determiner_plugins');
    }

    /**
     * Return plugin if group_membership_record_types contains the bundle or is true or empty
     *
     * @param   String  $group_membership_record_type  [$group_membership_record_type description]
     *
     * @return  [type]                             [return description]
     */
    public function getForInstanceType(String $group_membership_record_type)
    {
        $plugins = [];
        foreach ($this->getDefinitions() as $plugin_id => $plugin) {
            $plugin = $this->createInstance($plugin_id);
            if ($plugin->containsInstanceType($group_membership_record_type)) {
                $plugins[] = $plugin;
            }
        }
        return $plugins;
    }
}
