<?php

namespace Drupal\group_membership_record\Plugin\GroupMembershipRecord\EnabledDeterminer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\kst\KstPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\group\Entity\Group;
use Drupal\group_membership_record\Plugin\EnabledDeterminerPluginBase;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;

/**
 * Provides a meta tag generator plugin.
 *
 * @EnabledDeterminerPlugin(
 *   id = "enabled_determiner_default",
 *   description = @Translation("Leaves enabled to whatever it has been set."),
 *   group_membership_record_types = {}
 * )
 */
class DefaultEnabledDeterminer extends EnabledDeterminerPluginBase implements ContainerFactoryPluginInterface
{
    public $messageForFormEnabledField = false;

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition
        );
    }

    /**
     * {@inheritdoc}
     */
    public function determineEnabledValue(GroupMembershipRecord $record)
    {
        // No change
        return $record->get('enabled')->value;
    }

    public function getEnabledExplanation(GroupMembershipRecord $record) {
        return ['Set either manually or by default'];
    }
}
