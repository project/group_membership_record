<?php

namespace Drupal\group_membership_record\Plugin\views\field;

use DateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Get whether the user has a record for a grouip
 *
 * @ViewsField("group_membership_record_membership_check")
 */
class MembershipCheck extends FieldPluginBase
{
    /**
     * @{inheritdoc}
     */
    public function query()
    {
        // Leave empty to avoid a query on this field.
    }

    /**
     * {@inheritdoc}
     */
    protected function defineOptions()
    {
        $options = parent::defineOptions();

        $options['user'] = ['default' => null];
        $options['group'] = ['default' => null];
        $options['role'] = ['default' => null];
        $options['record_enabled'] = ['default' => TRUE];
        $options['record_current'] = ['default' => TRUE];
        $options['text_yes'] = ['default' => "Yes"];
        $options['text_no'] = ['default' => "No"];
        $options['text_unknown'] = ['default' => "Unknown"];

        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state)
    {
        parent::buildOptionsForm($form, $form_state);

        $fields = $this->displayHandler->getFieldLabels(TRUE);
        $form['group'] = [
            '#type' => 'select',
            '#title' => $this->t('View field referencing group ID of group to check'),
            '#default_value' => $this->options['group'],
            '#options' => $fields
        ];

        $form['user'] = [
            '#type' => 'select',
            '#title' => $this->t('View field referencing user ID of user to check'),
            '#default_value' => $this->options['user'],
            '#options' => $fields
        ];

        /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
        $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
        $roles = $gmrRepositoryService->getAllNonInternalRoles();
        $options = [];
        foreach ($roles as $role_id => $role) {
            $options[$role_id] = $role->label();
        }
        $form['role'] = [
            '#type' => 'select',
            '#title' => $this->t('Role to check if current'),
            '#default_value' => $this->options['role'],
            '#options' => $options
        ];

        $form['record_enabled'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Only count enabled records'),
            '#default_value' => $this->options['record_enabled']
        ];

        $form['record_current'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Only count current records'),
            '#default_value' => $this->options['record_current']
        ];

        $form['text_yes'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Text value for when record exists'),
            '#default_value' => $this->options['text_yes']
        ];
        $form['text_no'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Text value for when record does not exist'),
            '#default_value' => $this->options['text_no']
        ];
        $form['text_unknown'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Text value for when check fails'),
            '#default_value' => $this->options['text_unknown']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function render(ResultRow $values)
    {
        $alias = $this->field_alias;

        $group_id_field = 'groups_field_data_group_membership_record_' . $this->options['group'];
        $group_id = $values->$group_id_field;
        $uid_field = 'users_field_data_group_membership_record_' . $this->options['user'];
        $uid = $values->$uid_field;

        /** @var \Drupal\group\GroupInterface $group */

        if ($group_id && $group = Group::load($group_id)) {
        } else {
            $values->$alias = $this->options['text_unknown'] . " [group]";
            return parent::render($values);
        }

        if ($uid && $account = User::load($uid)) {
        } else {
            $values->$alias = $this->options['text_unknown'] . ' [user]';
            return parent::render($values);
        }

        $role = GroupRole::load($this->options['role']);

        $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
        $date = !empty($this->options['record_current']) ? new DateTime() : null;
        $records = $gmrRepositoryService->get($account, $group, $role, null, $date, !empty($this->options['record_enabled']));
        if (count($records) > 0) {
            $values->$alias = $this->options['text_yes'];
        } else {
            $values->$alias = $this->options['text_no'];
        }

        return parent::render($values);
    }
}
