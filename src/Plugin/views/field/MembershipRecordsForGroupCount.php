<?php

namespace Drupal\group_membership_record\Plugin\views\field;

use DateTime;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupRole;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Count of number of membership records for a group
 *
 * @ViewsField("group_membership_record_group_record_count")
 */
class MembershipRecordsForGroupCount extends NumericField {
  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['record_enabled'] = ['default' => TRUE];
    $options['record_current'] = ['default' => TRUE];
    $options['role'] = ['default' => []];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['record_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only count enabled records'),
      '#default_value' => $this->options['record_enabled']
    ];

    $form['record_current'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only count current records'),
      '#default_value' => $this->options['record_current']
    ];

    /** @var \Drupal\group_membership_record\Service\GroupMembershipRecordRepository $gmrRepositoryService */
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $roles = $gmrRepositoryService->getAllNonInternalRoles();
    $options = [];
    foreach ($roles as $role_id => $role) {
      $options[$role_id] = $role->label();
    }
    $form['role'] = [
      '#type' => 'select',
      '#title' => $this->t('Role(s) to check if current'),
      '#default_value' => $this->options['role'],
      '#options' => $options,
      '#multiple' => TRUE,
    ];
  }

  public function getRecords(ResultRow $values) {
    $group = $this->getEntity($values);
    $gmrRepositoryService = \Drupal::service('group_membership_record.repository');
    $date = !empty($this->options['record_current']) ? new DateTime() : NULL;
    $roles = !empty($this->options['role']) ? $this->options['role'] : NULL;
    if ($roles) {
      $records = [];
      foreach ($roles as $role_id) {
        $role = GroupRole::load($role_id);
        $records = array_merge($records, $gmrRepositoryService->get(NULL, $group, $role, NULL, $date, !empty($this->options['record_enabled'])));
      }
    } else {
      $records = $gmrRepositoryService->get(NULL, $group, NULL, NULL, $date, !empty($this->options['record_enabled']));
    }
    return $records;
  }

  /**
   * Filter out records if needed by extending fields.
   *
   * @param array $records
   *
   * @return array
   *   Of GMRs.
   */
  public function filterRecords(array $records) {
    return $records;
  }

  public function getCacheTags($values) {
    /** @var \Drupal\group\GroupInterface $group */
    $group = $this->getEntity($values);
    $tags = $group->getCacheTags();
    $tags[] = 'group:membership_records:' . $group->id();
    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    /** @var \Drupal\group\GroupInterface $group */
    $group = $this->getEntity($values);

    if ($group && $group->access('view')) {
      // Cache this value to avoid recalculating unecessarily for big views.
      $cid = __METHOD__ . $group->id();
      $options = '';
      foreach ($this->options as $key => $value) {
        if (is_array($value)) {
          $options .= print_r($value, TRUE);
        } else {
          $options .= $value;
        }
      }
      $cid .= hash('sha256', $options);

      if ($cache = \Drupal::cache()->get($cid)) {
        $count = $cache->data;
      } else {
        $records = $this->getRecords($values);
        $records = $this->filterRecords($records);
        $count = count($records);

        \Drupal::cache()->set($cid, $count, Cache::PERMANENT, $this->getCacheTags($values));
      }

      // return count
      $alias = $this->field_alias;
      $values->$alias = $count;
    }

    return parent::render($values);
  }
}
