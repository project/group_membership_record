<?php

namespace Drupal\group_membership_record\Plugin\views\field;

use DateTime;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\course\Entity\Course;
use Drupal\course\Entity\CourseEnrollment;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRole;
use Drupal\group_membership_record\Entity\GroupMembershipRecord;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Get whether the user has a record for a grouip
 *
 * @ViewsField("group_membership_record_check_enabled_summary")
 */
class EnabledCheckSummary extends FieldPluginBase {

  /**
   * @var \Drupal\group_membership_record\Entity\GroupMembershipRecord|null
   */
  public GroupMembershipRecord $record;

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['text_disabled'] = ['default' => "Not enabled"];
    $options['text_enabled'] = ['default' => "Enabled"];
    $options['show_full_explanation'] = ['default' => TRUE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['text_disabled'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text value for when disabled'),
      '#default_value' => $this->options['text_disabled']
    ];
    $form['text_enabled'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text value for when enabled'),
      '#default_value' => $this->options['text_enabled']
    ];
    $form['show_full_explanation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show full explanation'),
      '#default_value' => $this->options['show_full_explanation']
    ];
  }


  public function getEnabledDisabledText(bool $enabled) {
    if ($enabled) {
      $enabledText = t($this->options['text_enabled']) ?? t('Enabled');
    } else {
      $enabledText = t($this->options['text_disabled']) ?? t('Not enabled');
    }
    return $enabledText;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    /* @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $entity */
    $entity = $this->getEntity($values);
    $this->record = $entity;

    $enabled = NULL;
    $explanations = [];

    $pluginManager = \Drupal::service('plugin.manager.group_membership_record.enabled_determiner');
    $plugins = $pluginManager->getForInstanceType($this->record->bundle());
    foreach ($plugins as $plugin) {
      $enabled = $plugin->determineEnabledValue($this->record);
      $explanationsLoaded = array_merge($explanations, $plugin->getEnabledExplanation($this->record));
    }

    if ($this->record->enabled != $enabled) {
      $this->record->enabled = $enabled;
      $this->record->save();
    }

    $enabledText = $this->getEnabledDisabledText($enabled);

    $list = '';
    if($this->options['show_full_explanation']) {
      $explanations = [];
      foreach ($explanationsLoaded as $loaded) {
        if (is_array($loaded)) {
          if (isset($loaded[1])) {
            $success = isset($loaded[1]) && $loaded[1];
            $class = $success ? 'test-success' : 'test-failure';
            $emoji = $success ? '✔️' : '❌';
            $explanation = '<li class="test ' . $class . '">' . $emoji . ' ' . $loaded[0] . '</li>';
            if ($success || !isset($loaded[1])) {
              $explanations[] = $explanation;
            } else {
              array_unshift($explanations, $explanation);
            }
          } else {
            $explanations[] = '<li class="test">' . $loaded[0] . '</li>';
          }
        } else {
          $explanations[] = $loaded;
        }
      }

      $explanationsString = implode('', $explanations);

      $list = '<ul class="group_membership_record__enabled-test-results" data-read-more-label="See explanation">' . $explanationsString . '</ul>';
    }

    return new FormattableMarkup('@enabledText' . $list, ['@enabledText' => $enabledText]);
  }
}
