<?php

namespace Drupal\group_membership_record\Form;

use DateTime;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Class GroupMembershipRecordEndForm.
 */
class GroupMembershipRecordEndForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    // $form = parent::form($form, $form_state);

    $now = new DateTime();
    $form['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End date'),
      '#default_value' => implode('-', [
        'year' => date('Y', $now->getTimestamp()),
        'month' => date('m', $now->getTimestamp()),
        'day' => date('d', $now->getTimestamp()),
      ]),
      '#required' => TRUE,
    ];

    $form['actions']['delete']['#disabled']  = TRUE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $group_membership_record = $this->entity;

    $group_membership_record->setEndDate(
      strtotime($form_state->getValue('end_date'))
    );

    $status = $group_membership_record->save();

    switch ($status) {
      default:
        $this->messenger()->addMessage($this->t('Ended the %label.', [
          '%label' => $group_membership_record->type->entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($group_membership_record->getGroup()->toUrl('canonical'));
  }
}
