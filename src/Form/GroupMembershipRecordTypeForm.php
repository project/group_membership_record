<?php

namespace Drupal\group_membership_record\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GroupMembershipRecordTypeForm.
 */
class GroupMembershipRecordTypeForm extends EntityForm
{

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state)
  {
    $form = parent::form($form, $form_state);

    $group_membership_record_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $group_membership_record_type->label(),
      '#description' => $this->t("Label for the Group membership record type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $group_membership_record_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\group_membership_record\Entity\GroupMembershipRecordType::load',
      ],
      '#disabled' => !$group_membership_record_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    $form['allow_changing_core_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow user to change core fields (group, user, role) after creation'),
      '#description' => $this->t("Disable this to force users to create new records for new combinations of user+group+role. This means they would need to delete mistakes, rather than inadvertently overwriting a record through misunderstanding."),
      '#default_value' => $group_membership_record_type->get('allow_changing_core_fields') ?? true,
    ];

    $form['require_role'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require a role for each record'),
      '#description' => $this->t("This should nearly always be checked."),
      '#default_value' => $group_membership_record_type->get('require_role') ?? true,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $group_membership_record_type = $this->entity;
    $status = $group_membership_record_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Group membership record type.', [
          '%label' => $group_membership_record_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Group membership record type.', [
          '%label' => $group_membership_record_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($group_membership_record_type->toUrl('collection'));
  }
}
