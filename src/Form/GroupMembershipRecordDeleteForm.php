<?php

namespace Drupal\group_membership_record\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Group membership record entities.
 *
 * @ingroup group_membership_record
 */
class GroupMembershipRecordDeleteForm extends ContentEntityDeleteForm {


}
