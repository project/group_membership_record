<?php

/**
 * @file
 * Builds placeholder replacement tokens for gmr-related data.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_tokens().
 */
function group_membership_record_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  $url_options = ['absolute' => TRUE];
  $token_service = \Drupal::token();

  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = LanguageInterface::LANGCODE_DEFAULT;
  }

  if ($type == 'gmr' && !empty($data['gmr'])) {
    /** @var \Drupal\group_membership_record\Entity\GroupMembershipRecord $gmr */
    $gmr = $data['gmr'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'id':
          $replacements[$original] = $gmr->id();
          break;
      }
    }
  }

  return $replacements;
}
